#!/bin/bash

# Prends en entrée, un nombre de colonnes, un nombre de lignes, puis chaque
# lignes de 0 et .
# Un . est un trou, un 0 un nœud. Le but est d'indiquer sour la forme
# <nœud_x> <nœud_y> <rx> <ry> <bx> <by>
# avec x les colonnes et y les lignes, r étant le voisin de droite le plus
# proche et b celui du dessous. Si y'en a pas, on affiche -1 -1

# width: le nombre de colonnes
read width
# height: le nombre de lignes
read height
# on rempli le tableau
for (( i=0; i<height; i++ ))
do
    read line
    tab[$i]=$line
done

# C'est parti
# Pour chaque ligne
for (( i=0; i<height; i++ ))
do
    # On initialise.
    rx=-1
    ry=-1
    bx=-1
    by=-1
    
    # Pour chaque colonne
    for (( j=0; j<width; j++ ))
    do
        # Si le nœud existe
        # on compare la chaîne de longueur 1 commençant à la position j sur la
        # ligne i, et on vérifie qu'il vaut 0. Si oui, le nœud existe
        if [[ "${tab[$i]:$j:1}" = "0" ]] 
        then
            jj=$(( $j + 1 ))
            
            # On récupère la position X relative (1 étant juste à côté) du nœud
            # de droite le plus proche.
            rx=$(expr index "${tab[$i]:$jj}" "0")

            # Si on reçoit 0, c'est qu'il n'y en a pas
            if [[ $rx -eq 0 ]]
            then
                # Donc on dit qu'il y en a pas
                rx=-1
                ry=-1
            else
                # Sinon, on calcule la position X absolue (nœud actuel +
                # position relative)
                rx=$(( $rx + $j ))
                # Et on est sur la même ligne
                ry=$i
            fi
            
            # Celui du dessous maintenant
            # Pour chaque ligne en-dessous du nœud
            for (( k=$(( $i + 1 )); k<height; k++))
            do
                
                # Si y'a un nœud sur la même colonne
                if [[ ${tab[$k]:$j:1} = "0" ]]
                then
                    # On le garde, et on arrête de chercher
                    bx=$j
                    by=$k
                    break
                else
                    # Sinon, on mets qu'il n'y en a pas.
		    # Si c'est pas le dernier, ça va recommencer avec la ligne
		    # suivante, et potentiellement écraser cette valeur.
                    bx=-1
                    by=-1
                fi
            done
            
            # Et enfin, on affiche le résultat pour ce nœud.
            echo $j $i $rx $ry $bx $by
        fi
    done
done
